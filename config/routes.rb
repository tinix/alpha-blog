Rails.application.routes.draw do
  resources :comments
  resources :users, except: [:new]
  resources :articles
  resources :categories, except: [:destroy]

  get 'pages/home', to: 'pages#home'
  get 'about', to: 'pages#about'

  get 'signup', to: 'users#new'

  root to: 'pages#home'

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  
end
